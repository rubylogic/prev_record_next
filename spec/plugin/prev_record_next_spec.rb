require 'spec_helper'

describe PrevRecordNext do

  let(:task) { mock_model Task }
  let(:arguments) { { order: 'name ASC', project_id: 2, priority: 'high' } }

  describe '#next_record' do
    subject { task.next_record(arguments) }

    it { should_not be nil }
  end

  describe '#next_record?' do
    pending
  end

  describe '#prev_record' do
    pending
  end

  describe '#prev_record?' do
    pending
  end
end


require 'spec_helper'

describe CollectionOrder do

  let(:order_array) { [['name', 'ASC'], ['title', 'DESC'], ['created_at', 'ASC'], ] }

  describe '#initialize' do
    subject { described_class.new(order_array).order_array }

    it { should eql order_array }
  end

  describe '#direction' do
    context 'ascending' do
      subject { described_class.new(order_array).direction }

      it { should eql 'ASC' }
    end

    context 'descending' do
      before { order_array[0][1] = 'DESC' }
      subject { described_class.new(order_array).direction }

      it { should eql 'DESC' }
    end
  end

  describe '#column' do
    subject { described_class.new(order_array).column }

    it { should eql 'name' }
  end

  describe '#operator' do
    context 'ascending next_record' do
      subject { described_class.new(order_array).operator(true) }

      it { should eql '>' }
    end

    context 'descending next_record' do
      before { order_array[0][1] = 'DESC' }
      subject { described_class.new(order_array).operator(true) }

      it { should eql '<' }
    end

    context 'ascending prev_record' do
      subject { described_class.new(order_array).operator(false) }

      it { should eql '<' }
    end

    context 'descending prev_record' do
      before { order_array[0][1] = 'DESC' }
      subject { described_class.new(order_array).operator(false) }

      it { should eql '>' }
    end
  end

  describe '#query' do
    subject { described_class.new(order_array).query }

    it { should eql 'name ASC, title DESC, created_at ASC' }
  end

  describe '#clause' do
    context 'ascending next_record' do
      subject { described_class.new(order_array).clause(true) }

      it { should eql 'name > ?' }
    end

    context 'descending next_record' do
      before { order_array[0][1] = 'DESC' }
      subject { described_class.new(order_array).clause(true) }

      it { should eql 'name < ?' }
    end

    context 'ascending prev_record' do
      subject { described_class.new(order_array).clause(false) }

      it { should eql 'name < ?' }
    end

    context 'descending prev_record' do
      before { order_array[0][1] = 'DESC' }
      subject { described_class.new(order_array).clause(false) }

      it { should eql 'name > ?' }
    end
  end

  describe '#asc?' do
    context 'ascending' do
      subject { described_class.new(order_array).asc? }

      it { should be true }
    end

    context 'descending' do
      before { order_array[0][1] = 'DESC' }
      subject { described_class.new(order_array).asc? }

      it { should be false }
    end
  end

  describe '#desc?' do
    context 'ascending' do
      subject { described_class.new(order_array).desc? }

      it { should be false }
    end

    context 'descending' do
      before { order_array[0][1] = 'DESC' }
      subject { described_class.new(order_array).desc? }

      it { should be true }
    end
  end
end


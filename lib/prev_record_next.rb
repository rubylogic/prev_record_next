require 'prev_record_next/collection_order'

module PrevRecordNext
  extend ActiveSupport::Concern

  included do
  end

  def next_record(arguments={})
    order = CollectionOrder.parse(extract_from(arguments).order, true)
    self.
      class.
      range(extract_from(arguments).conditions).
      where(order.clause, self.send(order.column)).
      order(order.query).
      first
  end

  def prev_record(arguments={})
    order = CollectionOrder.parse(extract_from(arguments).order, false)
    self.
      class.
      range(extract_from(arguments).conditions).
      where(order.clause, self.send(order.column)).
      order(order.query).
      first
  end

  # element state
  def next_record?(arguments={})
    next_record(arguments).present?
  end

  def prev_record?(arguments={})
    prev_record(arguments).present?
  end

  module ClassMethods
    def range(arguments)
      return(self.unscoped) unless arguments.is_a?(Hash)

      query = self.unscoped

      arguments.each do |key, value|
        next unless value.present?

        if column_names.include?(key.to_s)
          query = query.where(key => value)
        elsif scopes[key.to_sym]
          query = query.send(key, value)
        end
      end

      query
    end
  end

  ActiveRecord::Base.send :include, PrevRecordNext

  private

  def extract_from(arguments)
    OpenStruct.new(
      :order => arguments[:order],
      :conditions => arguments.select{ |key| key != :order }
    )
  end
end

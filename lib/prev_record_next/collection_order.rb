class CollectionOrder

  #### Attributes
  attr_accessor :order_array

  #### Constructor
  def initialize(order_array, next_record)
    @next_record = next_record
    @order_array = order_array
    @reverse_order_array = reverse_order_array
  end

  #### Class methods
  def self.default(next_record)
    CollectionOrder.new([%w{id ASC}], next_record)
  end

  def self.parse(order, next_record)
    return CollectionOrder.default(next_record) if order.nil?

    order_array = (order.is_a?(Array) ? order.map(&:to_s) : order.to_s.split(',').map(&:strip)).
      map{ |field| field.downcase.match(/^\w* (asc|desc)$/) ? [field.split(' ')[0], field.split(' ')[1]] : [field, 'ASC'] }

    CollectionOrder.new(order_array, next_record)
  end

  #### Access methods
  def direction
    (@order_array[0][1] || 'ASC').upcase
  end

  def column
    @order_array[0][0] || 'id'
  end

  def operator
    if @next_record
      asc? ? '>' : '<'
    else
      desc? ? '>' : '<'
    end
  end

  def query
    array = @next_record ? @order_array : @reverse_order_array
    array.map{ |field| field.join(' ') }.join(', ')
  end

  def clause
    [column, operator, '?'].join(' ')
  end

  #### Element state methods
  def asc?
    direction == 'ASC'
  end

  def desc?
    !asc?
  end

  #### Private methods
  private

  def reverse_order_array
    @order_array.map{ |field| [field[0], (field[1] == 'ASC' ? 'DESC' : 'ASC')] }
  end
end

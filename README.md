Prev-Record-Next
====================

Prev-Record-Next plugin provides methods to find the predecessor and the successor of current record.

Installation
---------------------

Prev-Record-Next works fine with Rails 3.0 or higher. You can add it to your Gemfile with:

```ruby
gem 'prev_record_next', :git => 'https://bitbucket.org/rubylogic/prev_record_next.git'
```
And run the bundle command to install it.

Usage
---------------------

**Simple cases:**

```ruby
@current_record.next_record
#=> first record satisfying the condition id > @current_record.id

@current_record.prev_record
#=> first record satisfying the condition id < @current_record.id

@current_record.next_record?
#=> true or false depending if next_record exists or not

@current_record.prev_record?
#=> true or false depending if prev_record exists or not
```

**Ordering:**

You can provide :order key that defines collection ordering.

```ruby
@current_record.next_record(order: :name)
#=> Collection ordered by: name ASC

@current_record.next_record(order: 'name')
#=> Collection ordered by: name ASC

@current_record.next_record(order: 'name DESC')
#=> Collection ordered by: name DESC

@current_record.next_record(order: [:name, 'title DESC', :created_at])
#=> Collection ordered by: name DESC, title DESC, created_at ASC
```

**Narrowing scope:**

You can provide any key-value condition to narrow the collection.

```ruby
@current_record.next_record(project_id: 2, priority: 'high')
#=> Collection narrowed to records where: project_id = 2 AND priority = 'high'
```

License
---------------------

This project uses MIT-LICENSE.

Copyright 2013 RubyLogic http://rubylogic.pl

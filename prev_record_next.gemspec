$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "prev_record_next/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "prev_record_next"
  s.version     = PrevRecordNext::VERSION
  s.authors     = ["Alek Niemczyk"]
  s.email       = ["alek@rubylogic.pl"]
  s.homepage    = "https://bitbucket.org/rubylogic/prev_record_next"
  s.summary     = "Prev-Record-Next record finder"
  s.description = "Prev-Record-Next plugin provides methods to find the predecessor and the successor of current record."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "activerecord", "~> 3.0"

  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "capybara"
  s.add_development_dependency "guard-rspec"
  s.add_development_dependency "guard-spork"
  s.add_development_dependency "sqlite3"
end
